﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject_Team1
{
    public class Order
    {
        public int orderSN { get; private set; }
        public int memberSN { get; private set; }
        public string receiverName { get; private set; }
        public string receiverAddress { get; private set; }
        public string receiverPhone { get; private set; }
        public bool canceled { get; private set; }
        public bool paid { get; private set; }
        public bool picked_up { get; private set; }
        List<ItemInCart> buyList = new List<ItemInCart>();

        public Order(int order_sn, int member_sn, string receiver_name, string receiver_address, string receiver_phone, List<ItemInCart> buy_list, bool order_canceled = false)
        {
            orderSN = order_sn;
            memberSN = member_sn;
            receiverName = receiver_name;
            receiverAddress = receiver_address;
            receiverPhone = receiver_phone;
            buyList = buy_list;
            canceled = order_canceled;
        }

        public List<ItemInCart> GetBuyList()
        {
            return buyList;
        }

        public bool cancel()
        {
            canceled = true;
            return canceled;
        }

        public bool pay()
        {
            paid = true;
            return paid;
        }

        public bool pickup()
        {
            picked_up = true;
            return picked_up;
        }

    }
}
