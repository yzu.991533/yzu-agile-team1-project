﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject_Team1
{
    public class CartSystem
    {
        public int user_sn { get; private set; }
        private List<ItemInCart> cart = new List<ItemInCart>();

        public int SetUserSN(int member_sn, int member_count)
        {
            if (member_sn < 0 || member_sn > member_count) {return -1;}
            user_sn = member_sn;
            if (user_sn == member_sn) { return 0; }
            return 1;
        }

        public void PutInCart(int picked_itemSN, int picked_quantity, int picked_itemStcok)
        {
            if (picked_quantity < 1) { return; }
            if (picked_quantity > picked_itemStcok) { return; }
            for (int i = 0, size = cart.Count; i < size; i++)
            {
                if (cart[i].itemSN == picked_itemSN)
                {
                    cart[i].SetQuantity(picked_quantity);
                    return;
                }
            }

            ItemInCart temp = new ItemInCart(picked_itemSN, picked_quantity);
            cart.Add(temp);
        }

        public void DeleteFromCart(int index)
        {
            if (index < 0 || index >= cart.Count) { return; }

            cart.RemoveAt(index);
        }

        public void ModifyQuantity(int index, int new_quantity, ItemSystem item_Db)
        {
            if (new_quantity < 1) { return; }
            if (new_quantity > item_Db.GetItem(cart[index].itemSN).stock) { return; }

            cart[index].SetQuantity(new_quantity);
        }

        public int Buy(MemberSystem member_Db, OrderSystem order_Db, ItemSystem item_Db, string receiver_name = "", string receiver_address="", string receiver_phone="")
        {
            if (cart.Count == 0) { return -1; }

            for (int i = 0, size = cart.Count; i < size; i++)
            {
                if (cart[i].quantity > item_Db.GetItem(cart[i].itemSN).stock) { return -2; }
            }

            if (receiver_name == "")
            {
                receiver_name = member_Db.GetMemberName(user_sn);
            }
            if (receiver_address == "")
            {
                receiver_address = member_Db.GetMemberAddress(user_sn);
            }
            if (receiver_phone == "")
            {
                receiver_phone = member_Db.GetMemberPhone(user_sn);
            }
            int makeOrder = order_Db.AddOrder(user_sn, receiver_name, receiver_address, receiver_phone, cart);

            for (int j = 0, size = cart.Count; j < size; j++)
            {
                int number = item_Db.GetItem(cart[j].itemSN).stock - cart[j].quantity;
                item_Db.GetItem(cart[j].itemSN).SetStock(number);    
            }

            return makeOrder;
        }

        public List<ItemInCart> GetCart()
        {
            return cart;
        }
    }
}
