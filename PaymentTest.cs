﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
namespace FinalProject_Team1
{
    [TestFixture]
    class PaymentTest
    {
        [Test]
        public void SetPaymentTest()
        {
            Payment P1_payment = new Payment();

            P1_payment.SetPayPlaceChoose(0);
            Assert.That(P1_payment.PaymentPlace(), Is.EqualTo("ATM轉帳"));

            P1_payment.SetPayPlaceChoose(1);
            Assert.That(P1_payment.PaymentPlace(), Is.EqualTo("超商取貨付款"));

            P1_payment.SetPayPlaceChoose(2);
            Assert.That(P1_payment.PaymentPlace(), Is.EqualTo("住宅貨到付款"));

        }


    }
}
