﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject_Team1
{
    public class OrderSystem
    {
        private List<Order> orderDb = new List<Order>();

        public int AddOrder(int member_sn, string receiver_name, string receiver_address, string receiver_phone, List<ItemInCart> buy_list)
        {
            int order_sn = orderDb.Count;
            Order temp = new Order(order_sn, member_sn, receiver_name, receiver_address, receiver_phone, buy_list);
            orderDb.Add(temp);
            return order_sn;
        }

        public Order GetOrder(int order_sn)
        {
            for (int i = 0, size = orderDb.Count; i < size; i++)
            {
                if (orderDb[i].orderSN == order_sn)
                {
                    return orderDb[i];
                }
            }
            return null;
        }

        public List<Order> GetOrderDb()
        {
            return orderDb;
        }

        public int CancelOrder(int order_sn)
        {
            for (int i = 0, size = orderDb.Count; i < size; i++)
            {
                if (orderDb[i].orderSN == order_sn)
                {
                    orderDb[i].cancel();
                    if (orderDb[i].canceled == true) { return 0; }
                    else { return 1; }
                }
            }
            return -1;
        }

        public int PayOrder(int order_sn)
        {
            for (int i = 0, size = orderDb.Count; i < size; i++)
            {
                if (orderDb[i].orderSN == order_sn)
                {
                    if (orderDb[i].paid == false)
                    {
                        orderDb[i].pay();
                        if (orderDb[i].paid == true)
                        {
                            return 0;
                        }
                        return 1; 
                    }
                    return 1; 
                }
            }
            return -1;
        }

        public int PickUpOrder(int order_sn)
        {
            for (int i = 0, size = orderDb.Count; i < size; i++)
            {
                if (orderDb[i].orderSN == order_sn)
                {
                    if (orderDb[i].paid == true)
                    {
                        orderDb[i].pickup();
                    }
                    if (orderDb[i].picked_up == true) { return 0; }
                    else { return 1; }
                }
            }
            return -1;
        }
    }
}
