﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FinalProject_Team1
{
    public enum Category
    {
        Unknown = 0,
        Literature = 1,
        Art = 2,
        Lifestyle = 3,
        Travel = 4,
        Health = 5, 
        Social = 6,
        Science = 7,
        Finance = 8,
        Language = 9
    }
    public class Item
    {
        public int sn { get; private set; }
        public string name { get; private set; }
        public DateTime date { get; private set; }
        public string publisher { get; private set; }
        public string author { get; private set; }
        public string isbn10 { get; private set; }
        public string isbn13 { get; private set; }
        public string lang { get; private set; }
        public int version { get; private set; }
        public int pages { get; private set; }
        public double price { get; private set; }
        public double discount { get; private set; }
        public string desc { get; private set; }
        public string preview { get; private set; }
        public bool out_of_print { get; private set; }
        public int stock { get; private set; }
        public Category category { get; private set; }

        public Item(int item_sn, string item_name, DateTime item_date, string item_publisher, string item_author, string item_isbn10, string item_isbn13, string item_lang, int item_version, int item_pages, double item_price, double item_discount, string item_desc, string item_preview, bool item_out_of_print = false, int item_stock = 0, Category item_category = 0)
        {
            sn = item_sn;
            name = item_name;
            date = item_date;
            publisher = item_publisher;
            author = item_author;
            isbn10 = item_isbn10;
            isbn13 = item_isbn13;
            lang = item_lang;
            version = item_version;
            pages = item_pages;
            price = item_price;
            discount = item_discount;
            desc = item_desc;
            preview = item_preview;
            out_of_print = item_out_of_print;
            stock = item_stock;
            category = item_category;
        }

        public void SetStock(int number)
        {
            stock = number;
        }
    }
}
