﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace FinalProject_Team1
{
    [TestFixture]
    public class ItemTest
    {
        public ItemSystem SetStandardTestItemDB()
        {
            ItemSystem itemDb = new ItemSystem();
            itemDb.AddItem(
                name: "UNIX Network Programming",
                date: new DateTime(1990, 2, 2),
                publisher: "Prentice Hall",
                author: "W. Richard Stevens",
                isbn10: "0139498761",
                isbn13: "9780139498763",
                lang: "English",
                version: 1,
                pages: 768,
                price: 93.32,
                discount: 0.8,
                desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                preview: "",
                out_of_print: false,
                stock: 70,
                category: Category.Science
            );
            itemDb.AddItem(
                name: "Operating System Concepts",
                date: new DateTime(2012, 10, 10),
                publisher: "Wiley",
                author: "Abraham Silberschatz",
                isbn10: "1118063333",
                isbn13: "9781118063330",
                lang: "English",
                version: 1,
                pages: 944,
                price: 109.25,
                discount: 0.9,
                desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two.",
                out_of_print: false,
                stock: 18,
                category: Category.Science
            );
            itemDb.AddItem(
                name: "Computer Networks",
                date: new DateTime(2010, 10, 7),
                publisher: "PEARSON",
                author: "Andrew S. Tanenbaum, David J. Wetherall",
                isbn10: "0132126958",
                isbn13: "9780132126953",
                lang: "English",
                version: 5,
                pages: 960,
                price: 128.14,
                discount: 0.8,
                desc: @"Computer Networks, 5/e is appropriate for Computer Networking or Introduction to Networking courses at both the undergraduate and graduate level in Computer Science, Electrical Engineering, CIS, MIS, and Business Departments.
Tanenbaum takes a structured approach to explaining how networks work from the inside out. He starts with an explanation of the physical layer of networking, computer hardware and transmission systems; then works his way up to network applications. Tanenbaum's in-depth application coverage includes email; the domain name system; the World Wide Web (both client- and server-side); and multimedia (including voice over IP, Internet radio video on demand, video conferencing, and streaming media. Each chapter follows a consistent approach: Tanenbaum presents key principles, then illustrates them utilizing real-world example networks that run through the entire book—the Internet, and wireless networks, including Wireless LANs, broadband wireless and Bluetooth. The Fifth Edition includes a chapter devoted exclusively to network security. The textbook is supplemented by a Solutions Manual, as well as a Website containing PowerPoint slides, art in various forms, and other tools for instruction, including a protocol simulator whereby students can develop and test their own network protocols.",
                preview: @"Over the course of the years the cable system grew and the cables between the
various cities were replaced by high-bandwidth fiber, similar to what happened in
the telephone system. A system with fiber for the long-haul runs and coaxial
cable to the houses is called an HFC (Hybrid Fiber Coax) system. The electrooptical
converters that interface between the optical and electrical parts of the system
are called fiber nodes. Because the bandwidth of fiber is so much greater
than that of coax, a fiber node can feed multiple coaxial cables. Part of a modern
HFC system is shown in Fig. 2-51(a).
Over the past decade, many cable operators decided to get into the Internet
access business, and often the telephony business as well. Technical differences
between the cable plant and telephone plant had an effect on what had to be done
to achieve these goals. For one thing, all the one-way amplifiers in the system
had to be replaced by two-way amplifiers to support upstream as well as downstream
transmissions. While this was happening, early Internet over cable systems
used the cable television network for downstream transmissions and a dialup
connection via the telephone network for upstream transmissions. It was a
clever workaround, but not much of a network compared to what it could be.",
                out_of_print: false,
                stock: 38,
                category: Category.Science
            );
            return itemDb;
        }
        [Test]
        public void AddNormalItemTest()
        {
            ItemSystem itemDb = new ItemSystem();
            Assert.That(
                itemDb.AddItem(
                    name: "UNIX Network Programming",
                    date: new DateTime(1990, 2, 2),
                    publisher: "Prentice Hall",
                    author: "W. Richard Stevens",
                    isbn10: "0139498761",
                    isbn13: "9780139498763",
                    lang: "English",
                    version: 1,
                    pages: 768,
                    price: 93.32,
                    discount: 0.8,
                    desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                    preview: "",
                    category: Category.Science
                ), Is.EqualTo(0)
            );
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "Wiley",
                    author: "Abraham Silberschatz",
                    isbn10: "1118063333",
                    isbn13: "9781118063330",
                    lang: "English",
                    version: 1,
                    pages: 944,
                    price: 109.25,
                    discount: 0.9,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(1)
            );
        }
        [Test]
        public void AddDuplicatedItemTest()
        {
            ItemSystem itemDb = new ItemSystem();
            Assert.That(
                itemDb.AddItem(
                    name: "UNIX Network Programming",
                    date: new DateTime(1990, 2, 2),
                    publisher: "Prentice Hall",
                    author: "W. Richard Stevens",
                    isbn10: "0139498761",
                    isbn13: "9780139498763",
                    lang: "English",
                    version: 1,
                    pages: 768,
                    price: 93.32,
                    discount: 0.8,
                    desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                    preview: "",
                    category: Category.Science
                ), Is.EqualTo(0)
            );
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "Wiley",
                    author: "Abraham Silberschatz",
                    isbn10: "1118063333",
                    isbn13: "9781118063330",
                    lang: "English",
                    version: 1,
                    pages: 944,
                    price: 109.25,
                    discount: 0.9,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(1)
            );
            Assert.That(
            itemDb.AddItem(
                name: "UNIX Network Programming",
                date: new DateTime(1990, 2, 2),
                publisher: "Prentice Hall",
                author: "W. Richard Stevens",
                isbn10: "0139498761",
                isbn13: "9780139498763",
                lang: "English",
                version: 1,
                pages: 768,
                price: 93.32,
                discount: 0.8,
                desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                preview: "",
                category: Category.Science
            ), Is.EqualTo(-2000)
        );
        }
        [Test]
        public void AddAbnormalItemWithEmptyName()
        {
            ItemSystem itemDb = new ItemSystem();
            Assert.That(
                itemDb.AddItem(
                    name: "",
                    date: new DateTime(1990, 2, 2),
                    publisher: "Prentice Hall",
                    author: "W. Richard Stevens",
                    isbn10: "0139498761",
                    isbn13: "9780139498763",
                    lang: "English",
                    version: 1,
                    pages: 768,
                    price: 93.32,
                    discount: 0.8,
                    desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                    preview: ""
                ), Is.EqualTo(-2)
            );
        }
        [Test]
        public void AddAbnormalItemWithEmptyAuthor()
        {
            ItemSystem itemDb = new ItemSystem();
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "Wiley",
                    author: "",
                    isbn10: "1118063333",
                    isbn13: "9781118063330",
                    lang: "English",
                    version: 1,
                    pages: 944,
                    price: 109.25,
                    discount: 0.9,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(-16)
            );
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "Wiley",
                    author: "    ",
                    isbn10: "1118063333",
                    isbn13: "9781118063330",
                    lang: "English",
                    version: 1,
                    pages: 944,
                    price: 109.25,
                    discount: 0.9,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(-16)
            );
        }
        [Test]
        public void AddAbnormalItemWithEmptyPublisher()
        {
            ItemSystem itemDb = new ItemSystem();
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "    ",
                    author: "Abraham Silberschatz",
                    isbn10: "1118063333",
                    isbn13: "9781118063330",
                    lang: "English",
                    version: 1,
                    pages: 944,
                    price: 109.25,
                    discount: 0.9,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(-8)
            );
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "",
                    author: "Abraham Silberschatz",
                    isbn10: "1118063333",
                    isbn13: "9781118063330",
                    lang: "English",
                    version: 1,
                    pages: 944,
                    price: 109.25,
                    discount: 0.9,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(-8)
            );
        }
        [Test]
        public void AddAbnormalItemWithWrongFormattedISBN()
        {
            ItemSystem itemDb = new ItemSystem();
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "Wiley",
                    author: "Abraham Silberschatz",
                    isbn10: "111806333X",
                    isbn13: "9781118063330",
                    lang: "English",
                    version: 1,
                    pages: 944,
                    price: 109.25,
                    discount: 0.9,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(-32)
            );
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "Wiley",
                    author: "Abraham Silberschatz",
                    isbn10: "1118063333",
                    isbn13: "9781118063339",
                    lang: "English",
                    version: 1,
                    pages: 944,
                    price: 109.25,
                    discount: 0.9,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(-64)
            );
        }
        [Test]
        public void ISBN_ValidationTest()
        {
            ItemSystem itemDb = new ItemSystem();
            Assert.That(itemDb.ValidateISBN("1118063333", 10), Is.EqualTo(true));
            Assert.That(itemDb.ValidateISBN("1118063333", 13), Is.EqualTo(false));
            Assert.That(itemDb.ValidateISBN("9781118063330", 10), Is.EqualTo(false));
            Assert.That(itemDb.ValidateISBN("9781118063330", 13), Is.EqualTo(true));
            Assert.That(itemDb.ValidateISBN("99921-58-10-7", 10), Is.EqualTo(true));
            Assert.That(itemDb.ValidateISBN("99921-58-10-8", 10), Is.EqualTo(false));
            Assert.That(itemDb.ValidateISBN("0-8044-2957-x", 10), Is.EqualTo(true));
            Assert.That(itemDb.ValidateISBN("0-8044-2957-X", 10), Is.EqualTo(true));
            Assert.That(itemDb.ValidateISBN("0-8044-2957-0", 10), Is.EqualTo(false));
            Assert.That(itemDb.ValidateISBN("9971-5-0210-0", 10), Is.EqualTo(true));
            Assert.That(itemDb.ValidateISBN("9971-5-0210-1", 10), Is.EqualTo(false));
            Assert.That(itemDb.ValidateISBN("978-0-938692-16-4", 13), Is.EqualTo(true));
            Assert.That(itemDb.ValidateISBN("978-0-938692-16-5", 13), Is.EqualTo(false));
        }
        [Test]
        public void AddAbnormalItemWithEmptyLang()
        {
            ItemSystem itemDb = new ItemSystem();
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "Wiley",
                    author: "Abraham Silberschatz",
                    isbn10: "1118063333",
                    isbn13: "9781118063330",
                    lang: "",
                    version: 1,
                    pages: 944,
                    price: 109.25,
                    discount: 0.9,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(-128)
            );
        }
        [Test]
        public void AddAbnormalItemWithNagativeVersion()
        {
            ItemSystem itemDb = new ItemSystem();
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "Wiley",
                    author: "Abraham Silberschatz",
                    isbn10: "1118063333",
                    isbn13: "9781118063330",
                    lang: "English",
                    version: -1,
                    pages: 944,
                    price: 109.25,
                    discount: 0.9,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(-256)
            );
        }
        [Test]
        public void AddAbnormalItemWithNonPositivePage()
        {
            ItemSystem itemDb = new ItemSystem();
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "Wiley",
                    author: "Abraham Silberschatz",
                    isbn10: "1118063333",
                    isbn13: "9781118063330",
                    lang: "English",
                    version: 1,
                    pages: -1,
                    price: 109.25,
                    discount: 0.9,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(-512)
            );
        }
        [Test]
        public void AddAbnormalItemWithNonPositivePrice()
        {
            ItemSystem itemDb = new ItemSystem();
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "Wiley",
                    author: "Abraham Silberschatz",
                    isbn10: "1118063333",
                    isbn13: "9781118063330",
                    lang: "English",
                    version: 1,
                    pages: 994,
                    price: -99,
                    discount: 0.9,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(-1024)
            );
        }
        [Test]
        public void AddAbnormalItemWithWrongDiscount()
        {
            ItemSystem itemDb = new ItemSystem();
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "Wiley",
                    author: "Abraham Silberschatz",
                    isbn10: "1118063333",
                    isbn13: "9781118063330",
                    lang: "English",
                    version: 1,
                    pages: 994,
                    price: 109.25,
                    discount: 1.1,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(-2048)
            );
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "Wiley",
                    author: "Abraham Silberschatz",
                    isbn10: "1118063333",
                    isbn13: "9781118063330",
                    lang: "English",
                    version: 1,
                    pages: 994,
                    price: 109.25,
                    discount: -1.1,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(-2048)
            );
        }
        [Test]
        public void AddAbnormalItemWithNonPositiveStock()
        {
            ItemSystem itemDb = new ItemSystem();
            Assert.That(
                itemDb.AddItem(
                    name: "Operating System Concepts",
                    date: new DateTime(2012, 10, 10),
                    publisher: "Wiley",
                    author: "Abraham Silberschatz",
                    isbn10: "1118063333",
                    isbn13: "9781118063330",
                    lang: "English",
                    version: 1,
                    pages: 994,
                    price: 109.25,
                    stock: -1,
                    discount: 0.9,
                    desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                    preview: @"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."
                ), Is.EqualTo(-4096)
            );
        }
        [Test]
        public void GetItemListTest()
        {
            ItemSystem itemDb = SetStandardTestItemDB();
            Assert.That(itemDb.GetList(), Is.TypeOf(typeof(int[])));
            Assert.That(itemDb.GetList(), Has.No.Null);
            Assert.That(itemDb.GetList(), Is.Unique);
            Assert.That(itemDb.GetList().Length, Is.EqualTo(3));
            Assert.That(itemDb.GetList(), Is.All.GreaterThanOrEqualTo(0));
        }
        [Test]
        public void NormalSetItemTest()
        {
            Item temp = new Item(
                item_sn: 0,
                item_name: "UNIX Network Programming",
                item_date: new DateTime(1990, 2, 2),
                item_publisher: "Prentice Hall",
                item_author: "W. Richard Stevens",
                item_isbn10: "0139498761",
                item_isbn13: "9780139498763",
                item_lang: "English",
                item_version: 1,
                item_pages: 768,
                item_price: 93.32,
                item_discount: 0.8,
                item_desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                item_preview: "",
                item_category: Category.Science
            );
            Assert.That(temp.sn, Is.EqualTo(0));
        }
        [Test]
        public void ItemPreviewTest()
        {
            ItemSystem itemDb =SetStandardTestItemDB();
            Assert.That(itemDb.GetItem(1).preview, Is.EqualTo(@"An operating system is a program that manages a computer’s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two."));

        }
        [Test]
        public void GetItemTest()
        {
            ItemSystem itemDb = SetStandardTestItemDB();
            Item testItem = new Item(
                item_sn: 0,
                item_name: "UNIX Network Programming",
                item_date: new DateTime(1990, 2, 2),
                item_publisher: "Prentice Hall",
                item_author: "W. Richard Stevens",
                item_isbn10: "0139498761",
                item_isbn13: "9780139498763",
                item_lang: "English",
                item_version: 1,
                item_pages: 768,
                item_price: 93.32,
                item_discount: 0.8,
                item_desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                item_preview: "",
                item_out_of_print: false,
                item_category: Category.Science
            );

            Assert.That(itemDb.GetItem(0).name, Is.EqualTo(testItem.name));
            Assert.That(itemDb.GetItem(0).date, Is.EqualTo(testItem.date));
            Assert.That(itemDb.GetItem(0).publisher, Is.EqualTo(testItem.publisher));
            Assert.That(itemDb.GetItem(0).author, Is.EqualTo(testItem.author));
            Assert.That(itemDb.GetItem(0).isbn10, Is.EqualTo(testItem.isbn10));
            Assert.That(itemDb.GetItem(0).isbn13, Is.EqualTo(testItem.isbn13));
            Assert.That(itemDb.GetItem(0).lang, Is.EqualTo(testItem.lang));
            Assert.That(itemDb.GetItem(0).version, Is.EqualTo(testItem.version));
            Assert.That(itemDb.GetItem(0).pages, Is.EqualTo(testItem.pages));
            Assert.That(itemDb.GetItem(0).price, Is.EqualTo(testItem.price));
            Assert.That(itemDb.GetItem(0).discount, Is.EqualTo(testItem.discount));
            Assert.That(itemDb.GetItem(0).desc, Is.EqualTo(testItem.desc));
            Assert.That(itemDb.GetItem(0).preview, Is.EqualTo(testItem.preview));
            Assert.That(itemDb.GetItem(0).out_of_print, Is.EqualTo(testItem.out_of_print));
            Assert.That(itemDb.GetItem(0).category, Is.EqualTo(testItem.category));
        }
    }
}
