﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject_Team1
{
    class Member
    {
        public int sn { get; set; }
        public string id { get; set; }
        public string passwd { get; set; }
        public string name { get; set; }
        public DateTime birth { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public Member(int member_sn, string member_id, string member_passwd, string member_name, DateTime member_birth, string member_address, string member_phone, string member_email)
        {
            sn = member_sn;
            id = member_id;
            passwd = member_passwd;
            name = member_name;
            birth = member_birth;
            address = member_address;
            phone = member_phone;
            email = member_email;
        }
    }
}
