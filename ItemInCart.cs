﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject_Team1
{
    public class ItemInCart
    {
        public int itemSN { get; private set; }
        public int quantity { get; private set; }

        public ItemInCart(int picked_itemSN, int picked_quantity)
        {
            itemSN = picked_itemSN;
            quantity = picked_quantity;
        }

        public void SetQuantity(int new_quantity)
        {
            quantity = new_quantity;
        }
    }
}
