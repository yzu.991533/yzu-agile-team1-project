using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace FinalProject_Team1
{
    [TestFixture]
    class CartTest
    {
        public MemberSystem SetStandardMemberDB()
        {
            MemberSystem memberDb = new MemberSystem();
            memberDb.AddAccount("testid", "passsssword", "Jason", new DateTime(1992, 2, 15), "Room 1201, Boy Dorm, 135 Yuan-Tung Road, Chung-Li, Taiwan 32003, R.O.C.", "0987987987", "jasonwu@mail.yzu.edu.tw");
            memberDb.AddAccount("testid2", "passsssword", "Tim", new DateTime(1993, 11, 2), "Room 3311, Boy Dorm, 135 Yuan-Tung Road, Chung-Li, Taiwan 32003, R.O.C.", "0981235335", "timwang@mail.yzu.edu.tw");
            memberDb.AddAccount("testid3", "passsssword", "Gina", new DateTime(1989, 7, 31), "Room 2611, Boy Dorm, 135 Yuan-Tung Road, Chung-Li, Taiwan 32003, R.O.C.", "0923464523", "ginalin@mail.yzu.edu.tw");
            return memberDb;
        }
        [Test]
        public void SetNormalMemberInCartTest()
        {
            MemberSystem memberDb = SetStandardMemberDB();
            CartSystem shoppingCart = new CartSystem();
            Assert.That(shoppingCart.SetUserSN(2, memberDb.GetNumberOfMember()), Is.EqualTo(0));
        }
        [Test]
        public void SetNonExistMemberInCartTest()
        {
            MemberSystem memberDb = SetStandardMemberDB();
            CartSystem shoppingCart = new CartSystem();
            Assert.That(shoppingCart.SetUserSN(999, memberDb.GetNumberOfMember()), Is.EqualTo(-1));
        }
        [Test]
        public void NormalPutInCartTest()
        {
            MemberSystem memberDb = SetStandardMemberDB();
            CartSystem shoppingCart = new CartSystem();

            Item item1 = new Item(
                item_sn: 0,
                item_name: "Test 1",
                item_date: new DateTime(1990, 2, 2),
                item_publisher: "Prentice Hall",
                item_author: "W. Richard Stevens",
                item_isbn10: "0139498761",
                item_isbn13: "9780139498763",
                item_lang: "English",
                item_version: 1,
                item_pages: 768,
                item_price: 93.32,
                item_discount: 0.8,
                item_desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                item_preview: "",
                item_out_of_print: false,
                item_stock: 70,
                item_category: Category.Science
            );

            int User1 = memberDb.SignIn("testid2", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            Assert.That(shoppingCart.user_sn, Is.EqualTo(1));

            shoppingCart.PutInCart(item1.sn, 2, item1.stock);
            Assert.That(shoppingCart.GetCart()[0].itemSN, Is.EqualTo(item1.sn));
            Assert.That(shoppingCart.GetCart()[0].quantity, Is.EqualTo(2));
        }
        [Test]
        public void NormalDeleteFromCartTest()
        {
            MemberSystem memberDb = SetStandardMemberDB();
            CartSystem shoppingCart = new CartSystem();

            Item item1 = new Item(
                item_sn: 0,
                item_name: "Test 1",
                item_date: new DateTime(1990, 2, 2),
                item_publisher: "Prentice Hall",
                item_author: "W. Richard Stevens",
                item_isbn10: "0139498761",
                item_isbn13: "9780139498763",
                item_lang: "English",
                item_version: 1,
                item_pages: 768,
                item_price: 93.32,
                item_discount: 0.8,
                item_desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                item_preview: "",
                item_out_of_print: false,
                item_stock: 70,
                item_category: Category.Science
            );
            Item item2 = new Item(
                item_sn: 1,
                item_name: "Test 2",
                item_date: new DateTime(1990, 2, 2),
                item_publisher: "Prentice Hall",
                item_author: "W. Richard Stevens",
                item_isbn10: "0139498761",
                item_isbn13: "9780139498763",
                item_lang: "English",
                item_version: 1,
                item_pages: 768,
                item_price: 93.32,
                item_discount: 0.8,
                item_desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                item_preview: "",
                item_out_of_print: false,
                item_stock: 70,
                item_category: Category.Science
            );

            int User1 = memberDb.SignIn("testid2", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            Assert.That(shoppingCart.user_sn, Is.EqualTo(1));

            shoppingCart.PutInCart(item1.sn, 2, item1.stock);
            shoppingCart.PutInCart(item2.sn, 1, item2.stock);
            shoppingCart.DeleteFromCart(index: 0);
            Assert.That(shoppingCart.GetCart()[0].itemSN, Is.EqualTo(item2.sn));
            Assert.That(shoppingCart.GetCart()[0].quantity, Is.EqualTo(1));
        }
        [Test]
        public void DeleteNonExistItemFromCartTest()
        {
            MemberSystem memberDb = SetStandardMemberDB();
            CartSystem shoppingCart = new CartSystem();

            Item item1 = new Item(
                item_sn: 0,
                item_name: "Test 1",
                item_date: new DateTime(1990, 2, 2),
                item_publisher: "Prentice Hall",
                item_author: "W. Richard Stevens",
                item_isbn10: "0139498761",
                item_isbn13: "9780139498763",
                item_lang: "English",
                item_version: 1,
                item_pages: 768,
                item_price: 93.32,
                item_discount: 0.8,
                item_desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                item_preview: "",
                item_out_of_print: false,
                item_stock: 70,
                item_category: Category.Science
            );
            Item item2 = new Item(
                item_sn: 1,
                item_name: "Test 2",
                item_date: new DateTime(1990, 2, 2),
                item_publisher: "Prentice Hall",
                item_author: "W. Richard Stevens",
                item_isbn10: "0139498761",
                item_isbn13: "9780139498763",
                item_lang: "English",
                item_version: 1,
                item_pages: 768,
                item_price: 93.32,
                item_discount: 0.8,
                item_desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                item_preview: "",
                item_out_of_print: false,
                item_stock: 70,
                item_category: Category.Science
            );

            int User1 = memberDb.SignIn("testid2", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            Assert.That(shoppingCart.user_sn, Is.EqualTo(1));

            shoppingCart.PutInCart(item1.sn, 2, item1.stock);
            shoppingCart.PutInCart(item2.sn, 1, item2.stock);
            shoppingCart.DeleteFromCart(index: 50);
            shoppingCart.DeleteFromCart(index: -7);
            Assert.That(shoppingCart.GetCart().Count, Is.EqualTo(2));
        }
        [Test]
        public void ChangeQuantityOfItemInCartTest()
        {
            MemberSystem memberDb = SetStandardMemberDB();
            CartSystem shoppingCart = new CartSystem();
            ItemSystem itemDb = new ItemSystem();

            itemDb.AddItem(
                name: "UNIX Network Programming",
                date: new DateTime(1990, 2, 2),
                publisher: "Prentice Hall",
                author: "W. Richard Stevens",
                isbn10: "0139498761",
                isbn13: "9780139498763",
                lang: "English",
                version: 1,
                pages: 768,
                price: 93.32,
                discount: 0.8,
                desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                preview: "",
                out_of_print: false,
                stock: 70,
                category: Category.Science
            );

            int User1 = memberDb.SignIn("testid2", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            Assert.That(shoppingCart.user_sn, Is.EqualTo(1));

            shoppingCart.PutInCart(itemDb.GetItem(0).sn, 2, itemDb.GetItem(0).stock);
            Assert.That(shoppingCart.GetCart()[0].itemSN, Is.EqualTo(itemDb.GetItem(0).sn));
            Assert.That(shoppingCart.GetCart()[0].quantity, Is.EqualTo(2));

            shoppingCart.ModifyQuantity(index: 0, new_quantity: 5, item_Db: itemDb);
            Assert.That(shoppingCart.GetCart()[0].itemSN, Is.EqualTo(itemDb.GetItem(0).sn));
            Assert.That(shoppingCart.GetCart()[0].quantity, Is.EqualTo(5));

            shoppingCart.ModifyQuantity(index: 0, new_quantity: -5, item_Db: itemDb);
            Assert.That(shoppingCart.GetCart()[0].itemSN, Is.EqualTo(itemDb.GetItem(0).sn));
            Assert.That(shoppingCart.GetCart()[0].quantity, Is.EqualTo(5));

            shoppingCart.ModifyQuantity(index: 0, new_quantity: 99999, item_Db: itemDb);
            Assert.That(shoppingCart.GetCart()[0].itemSN, Is.EqualTo(itemDb.GetItem(0).sn));
            Assert.That(shoppingCart.GetCart()[0].quantity, Is.EqualTo(5));
        }
        [Test]
        public void PutItemAlreadyExistInCartTest()
        {
            MemberSystem memberDb = SetStandardMemberDB();
            CartSystem shoppingCart = new CartSystem();

            Item item1 = new Item(
                item_sn: 0,
                item_name: "Test 1",
                item_date: new DateTime(1990, 2, 2),
                item_publisher: "Prentice Hall",
                item_author: "W. Richard Stevens",
                item_isbn10: "0139498761",
                item_isbn13: "9780139498763",
                item_lang: "English",
                item_version: 1,
                item_pages: 768,
                item_price: 93.32,
                item_discount: 0.8,
                item_desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                item_preview: "",
                item_out_of_print: false,
                item_stock: 70,
                item_category: Category.Science
            );

            int User1 = memberDb.SignIn("testid2", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            Assert.That(shoppingCart.user_sn, Is.EqualTo(1));

            shoppingCart.PutInCart(item1.sn, 2, item1.stock);
            shoppingCart.PutInCart(item1.sn, 1, item1.stock);
            Assert.That(shoppingCart.GetCart()[0].itemSN, Is.EqualTo(item1.sn));
            Assert.That(shoppingCart.GetCart()[0].quantity, Is.EqualTo(1));
            Assert.That(shoppingCart.GetCart().Count, Is.EqualTo(1));
        }
        [Test]
        public void PutItemInCartWithAbnormalQuantityTest()
        {
            MemberSystem memberDb = SetStandardMemberDB();
            CartSystem shoppingCart = new CartSystem();

            Item item1 = new Item(
                item_sn: 0,
                item_name: "Test 1",
                item_date: new DateTime(1990, 2, 2),
                item_publisher: "Prentice Hall",
                item_author: "W. Richard Stevens",
                item_isbn10: "0139498761",
                item_isbn13: "9780139498763",
                item_lang: "English",
                item_version: 1,
                item_pages: 768,
                item_price: 93.32,
                item_discount: 0.8,
                item_desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                item_preview: "",
                item_out_of_print: false,
                item_stock: 70,
                item_category: Category.Science
            );

            int User1 = memberDb.SignIn("testid2", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            Assert.That(shoppingCart.user_sn, Is.EqualTo(1));

            shoppingCart.PutInCart(item1.sn, -2, item1.stock);
            shoppingCart.PutInCart(item1.sn, 71, item1.stock);
            Assert.That(shoppingCart.GetCart().Count, Is.EqualTo(0));
            shoppingCart.PutInCart(item1.sn, 70, item1.stock);
            Assert.That(shoppingCart.GetCart().Count, Is.EqualTo(1));
        }
    }
}

