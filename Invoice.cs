﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject_Team1
{
    class Invoice
    {
        public bool IsDonate { get; private set; }
        public int DonatePlaceChioce { get; private set; }
        public String[] DonatePlace { get; private set; }
        public static int InvoiceNumber { get; private set; }

        public Invoice()
        {
            IsDonate = false;
            DonatePlaceChioce = 0;
            DonatePlace = new String[] {"NO Donate" ,"創世基金會", "RedCross", "婦女互助會" };
        }

        public void SetIsDonate(bool DonateWill)
        {
            IsDonate = DonateWill;
        }

        public void SetDonateChoice(int UserDonateChoice)
        {
            DonatePlaceChioce = UserDonateChoice;
        }

        public string ChooseDonatePlace()
        {
            if (IsDonate == true)
            {
                return DonatePlace[DonatePlaceChioce];
            }
            else
            {
                return DonatePlace[0];
            }
        }
    }
}
