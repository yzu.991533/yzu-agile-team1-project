﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FinalProject_Team1
{
    public class ItemSystem
    {
        List<Item> itemList = new List<Item>();
        public int AddItem(string name, DateTime date, string publisher, string author, string isbn10, string isbn13, string lang, int version, int pages, double price, double discount, string desc, string preview, bool out_of_print = false, int stock = 0, Category category = Category.Unknown)
        {
            int sn = itemList.Count;
            int errorNum = 0;
            if ("" == name.Trim()) { errorNum -= 2; }
            if ("" == publisher.Trim()) { errorNum -= 8; }
            if ("" == author.Trim()) { errorNum -= 16; }
            if (!ValidateISBN(isbn10)) { errorNum -= 32; }
            if (!ValidateISBN(isbn13, 13)) { errorNum -= 64; }
            if ("" == lang.Trim()) { errorNum -= 128; }
            if (version < 0) { errorNum -= 256; }
            if (pages < 0) { errorNum -= 512; }
            if (price < 0) { errorNum -= 1024; }
            if (discount < 0 || discount > 1) { errorNum -= 2048; }
            if (stock < 0) { errorNum -= 4096; }
            if (category < 0) { errorNum -= 8192; }
            if (errorNum != 0) { return errorNum; }
            if (exist(isbn10.Trim().Replace("-", ""), isbn13.Trim().Replace("-", ""))) { return -2000; }
            Item temp = new Item(sn, name.Trim(), date, publisher.Trim(), author.Trim(), isbn10.Trim().Replace("-", ""), isbn13.Trim().Replace("-", ""), lang.Trim(), version, pages, price, discount, desc.Trim(), preview.Trim(), out_of_print, stock, category);
            itemList.Add(temp);
            return (sn + 1 == itemList.Count)? sn : -999;
        }

        public bool exist(string isbn10, string isbn13)
        {
            for (int i = 0, size = itemList.Count; i < size; i++)
            {
                if ((itemList[i].isbn10 == isbn10) || (itemList[i].isbn13 == isbn13))
                {
                    return true;
                }
            }
            return false;
        }
        public bool ValidateISBN(string isbn, int type = 10)
        {
            isbn = isbn.Trim().Replace("-", "");
            if (isbn.Length != type) { return false; }
            if (10 == type)
            {
                int S = (isbn[0] - '0') * 10 + (isbn[1] - '0') * 9 + (isbn[2] - '0') * 8 + (isbn[3] - '0') * 7 + (isbn[4] - '0') * 6 + (isbn[5] - '0') * 5 + (isbn[6] - '0') * 4 + (isbn[7] - '0') * 3 + (isbn[8] - '0') * 2;
                int M = S % 11;
                int N = 11 - M;
                switch (N)
                {
                    case 10:
                        return ('X' == isbn[9] || 'x' == isbn[9]) ? true : false;
                    case 11:
                        return (0 == isbn[9] - '0') ? true : false;
                    default:
                        return (N == isbn[9] - '0') ? true : false;
                }
            }
            else
            {
                int S = (isbn[0] - '0') + (isbn[1] - '0') * 3 + (isbn[2] - '0') + (isbn[3] - '0') * 3 + (isbn[4] - '0') + (isbn[5] - '0') * 3 + (isbn[6] - '0') + (isbn[7] - '0') * 3 + (isbn[8] - '0') + (isbn[9] - '0') * 3 + (isbn[10] - '0') + (isbn[11] - '0') * 3;
                int M = S % 10;
                int N = 10 - M;
                if (10 == N) { return (0 == isbn[12] - '0') ? true : false; }
                return (N == isbn[12] - '0') ? true : false;
            }
        }

        public int[] GetList()
        {
            List<int> tempList = new List<int>();
            for (int i = 0, size = itemList.Count; i < size; i++)
            {
                tempList.Add(itemList[i].sn);
            }
            return tempList.ToArray();
        }

        public Item GetItem(int sn)
        {
            return itemList[sn];
        }

        public bool SearchString(string keyword, string target)
        {
            for (int i = 0; i < target.Length; i++)
            {
                bool match = true;
                int j;
                for (j = 0; j < keyword.Length && i + j < target.Length; j++)
                {
                    if(Char.ToLower(keyword[j]) != Char.ToLower(target[i+j])) {
                        match = false;
                        break;
                    }
                }
                if (match && j == keyword.Length) return true;
            }
            return false;
        }

        public List<Item> SearchItem(string keyword)
        {
            List<Item> matchItem = new List<Item>();

            for (int i = 0, size = itemList.Count; i < size && keyword != ""; i++)
            {
                if (SearchProperty(keyword, itemList[i]))
                {
                    matchItem.Add(itemList[i]);
                }
            }

            return matchItem;
        }

        public List<Item> SearchItem(string keyword, Category category)
        {
            List<Item> matchItem = new List<Item>();

            for (int i = 0, size = itemList.Count; i < size && keyword != ""; i++)
            {
                if ((itemList[i].category == Category.Unknown || itemList[i].category == category)
                    && SearchProperty(keyword, itemList[i]))
                {
                    matchItem.Add(itemList[i]);
                }
            }

            return matchItem;
        }

        public List<Item> SearchItem(string keyword, double upperBound, double lowerBound)
        {
            List<Item> matchItem = new List<Item>();

            for (int i = 0, size = itemList.Count; i < size && keyword != ""; i++)
            {
                if (lowerBound <= itemList[i].price * itemList[i].discount && itemList[i].price * itemList[i].discount <= upperBound )
                {
                    if (SearchProperty(keyword, itemList[i]))
                    {
                        matchItem.Add(itemList[i]);
                    }
                }
            }

            return matchItem;
        }

        public List<Item> SearchItem(string keyword, Category category, double upperBound, double lowerBound)
        {
            List<Item> matchItem = new List<Item>();

            for (int i = 0, size = itemList.Count; i < size && keyword != ""; i++)
            {
                if (lowerBound <= itemList[i].price * itemList[i].discount && itemList[i].price * itemList[i].discount <= upperBound
                    && (itemList[i].category == Category.Unknown || itemList[i].category == category))
                {
                    if (SearchProperty(keyword, itemList[i]))
                    {
                        matchItem.Add(itemList[i]);
                    }
                }
            }

            return matchItem;
        }

        public bool SearchProperty(string keyword, Item item)
        {
            if (string.Equals(item.isbn10, keyword))
            {
                return true;
            }
            else if (string.Equals(item.isbn13, keyword))
            {
                return true;
            }
            else if (SearchString(keyword, item.name))
            {
                return true;
            }
            else if (SearchString(keyword, item.publisher))
            {
                return true;
            }
            else if (SearchString(keyword, item.author))
            {
                return true;
            }
            else if (SearchString(keyword, item.desc))
            {
                return true;
            }
            return false;
        }

        public List<Item> LatestProducts()
        {
            List<Item> temp = new List<Item>();

            return temp;
        }
    }
    
    
}
