﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions; 
using System.Linq;
using System.Text;

namespace FinalProject_Team1
{
    public class MemberSystem
    {
        List<Member> memberList = new List<Member>();
        public int AddAccount(string id, string passwd, string name, DateTime birth, string address, string phone, string email)
        {
            int sn = memberList.Count;
            int errorNum = 0;
            if ("" == id.Trim()) { errorNum -= 2; }
            if ("" == passwd.Trim()) { errorNum -= 4; }
            if ("" == name.Trim()) { errorNum -= 8; }
            if ("" == address.Trim()) { errorNum -= 32; }
            if (PhoneValidation(phone) == false) { errorNum -= 64; }
            if (EmailValidation(email) == false) { errorNum -= 128; }
            if (errorNum != 0) { return errorNum; }
            if (Duplicated(id.Trim(), phone.Trim(), email.Trim()) != false) { return -2000; }
            Member temp = new Member(sn, id, passwd, name, birth, address, phone, email);
            memberList.Add(temp);
            return (sn + 1 == memberList.Count) ? sn : -999;
        }

        static public bool EmailValidation(string email)
        {
            return Regex.IsMatch(email.Trim(), @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }

        static public bool PhoneValidation(string phone)
        {
            return Regex.IsMatch(phone.Trim(), @"[(]?[0-9]{3,}[0-9#-()]*", RegexOptions.IgnorePatternWhitespace);
        }

        private bool Duplicated(string id, string phone, string email)
        {
            for (int i = 0, size = memberList.Count; i < size; i++)
            {
                if (string.Equals(memberList[i].id, id) || string.Equals(memberList[i].phone, phone) || string.Equals(memberList[i].email, email))
                {
                    return true;
                }
            }
            return false;
        }

        public int SignIn(string id, string passwd)
        {
            for (int i = 0, size = memberList.Count; i < size; i++)
            {
                if (string.Equals(memberList[i].id, id))
                {
                    if (string.Equals(memberList[i].passwd, passwd))
                    {
                        return memberList[i].sn;
                    }
                    else
                    {
                        return -2;
                    }
                }
            }
            return -1;
        }

        public int GetNumberOfMember()
        {
            return memberList.Count();
        }

        public string GetMemberName(int sn)
        {
            for (int i = 0, size = memberList.Count; i < size; i ++ )
            {
                if (memberList[i].sn == sn)
                {
                    return memberList[sn].name;
                }
            }
            return null;
        }

        public string GetMemberAddress(int sn)
        {
            for (int i = 0, size = memberList.Count; i < size; i++)
            {
                if (memberList[i].sn == sn)
                {
                    return memberList[sn].address;
                }
            }
            return null;
        }

        public string GetMemberPhone(int sn)
        {
            for (int i = 0, size = memberList.Count; i < size; i++)
            {
                if (memberList[i].sn == sn)
                {
                    return memberList[sn].phone;
                }
            }
            return null;
        }

        public DateTime GetMemberBirth(int sn)
        {
            for (int i = 0, size = memberList.Count; i < size; i++)
            {
                if (memberList[i].sn == sn)
                {
                    return memberList[sn].birth;
                }
            }
            return new DateTime();
        }

        public string GetMemberEmail(int sn)
        {
            for (int i = 0, size = memberList.Count; i < size; i++)
            {
                if (memberList[i].sn == sn)
                {
                    return memberList[sn].email;
                }
            }
            return null;
        }
        public double GetPayRate(int sn, List<Order> orderDb)
        {
            UInt32 totalOrder = 0, totalPaid = 0;
            for (int i = 0, size = orderDb.Count; i < size; i++)
            {
                if (orderDb[i].memberSN == sn)
                {
                    totalOrder++;
                    if (orderDb[i].paid)
                    {
                        totalPaid++;
                    }
                }
            }
            if (totalOrder == 0)
            {
                return 0.0;
            }
            return ((double)totalPaid/totalOrder)*100.0;
        }

        public double GetPickRate(int sn, List<Order> orderDb)
        {
            UInt32 totalOrder = 0, totalPick = 0;
            for (int i = 0, size = orderDb.Count; i < size; i++)
            {
                if (orderDb[i].memberSN == sn)
                {
                    totalOrder++;
                    if (orderDb[i].picked_up)
                    {
                        totalPick++;
                    }
                }
            }
            if (totalOrder == 0)
            {
                return 0.0;
            }
            return ((double)totalPick / totalOrder) * 100.0;
        }
    }
}
