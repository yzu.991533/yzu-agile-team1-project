using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace FinalProject_Team1
{
    [TestFixture]
    class OrderTest
    {
        public MemberSystem SetNormalMemberDB()
        {
            MemberSystem memberDb = new MemberSystem();
            memberDb.AddAccount("testid", "passsssword", "Jason", new DateTime(1992, 2, 15), "Room 1201, Boy Dorm, 135 Yuan-Tung Road, Chung-Li, Taiwan 32003, R.O.C.", "0987987987", "jasonwu@mail.yzu.edu.tw");
            memberDb.AddAccount("testid2", "passsssword", "Tim", new DateTime(1993, 11, 2), "Room 3311, Boy Dorm, 135 Yuan-Tung Road, Chung-Li, Taiwan 32003, R.O.C.", "0981235335", "timwang@mail.yzu.edu.tw");
            memberDb.AddAccount("testid3", "passsssword", "Gina", new DateTime(1989, 7, 31), "Room 2611, Boy Dorm, 135 Yuan-Tung Road, Chung-Li, Taiwan 32003, R.O.C.", "0923464523", "ginalin@mail.yzu.edu.tw");
            return memberDb;
        }
        public ItemSystem SetStandardTestItemDB()
        {
            ItemSystem itemDb = new ItemSystem();
            itemDb.AddItem(
                name: "UNIX Network Programming",
                date: new DateTime(1990, 2, 2),
                publisher: "Prentice Hall",
                author: "W. Richard Stevens",
                isbn10: "0139498761",
                isbn13: "9780139498763",
                lang: "English",
                version: 1,
                pages: 768,
                price: 93.32,
                discount: 0.8,
                desc: @"The leading book in its field, this guide focuses on the design, development and coding of network software under the UNIX operating system. Provides over 15,000 lines of C code with descriptions of how and why a given solution is achieved. For programmers seeking an indepth tutorial on sockets, transport level interface (TLI), interprocess communications (IPC) facilities under System V and BSD UNIX.",
                preview: "",
                out_of_print: false,
                stock: 70,
                category: Category.Science
            );
            itemDb.AddItem(
                name: "Operating System Concepts",
                date: new DateTime(2012, 10, 10),
                publisher: "Wiley",
                author: "Abraham Silberschatz",
                isbn10: "1118063333",
                isbn13: "9781118063330",
                lang: "English",
                version: 1,
                pages: 944,
                price: 109.25,
                discount: 0.9,
                desc: @"Operating System Concepts, now in its ninth edition, continues to provide a solid theoretical foundation for understanding operating systems. The ninth edition has been thoroughly updated to include contemporary examples of how operating systems function. The text includes content to bridge the gap between concepts and actual implementations. End-of-chapter problems, exercises, review questions, and programming exercises help to further reinforce important concepts. A new Virtual Machine provides interactive exercises to help engage students with the material.",
                preview: @"An operating system is a program that manages a computer��s hardware. It also provides a basis for application programs and acts as an intermediary between the computer user and the computer hardware. An amazing aspect of operating systems is how they vary in accomplishing these tasks. Mainframe operating systems are designed primarily to optimize utilization of hardware. Personal computer (PC) operating systems support complex games, business applications, and everything in between. Operating systems for mobile computers provide an environment in which a user can easily interface with the computer to execute programs. Thus, some operating systems are designed to be convenient, others to be efficient, and others to be some combination of the two.",
                out_of_print: false,
                stock: 18,
                category: Category.Science
            );
            itemDb.AddItem(
                name: "Computer Networks",
                date: new DateTime(2010, 10, 7),
                publisher: "PEARSON",
                author: "Andrew S. Tanenbaum, David J. Wetherall",
                isbn10: "0132126958",
                isbn13: "9780132126953",
                lang: "English",
                version: 5,
                pages: 960,
                price: 128.14,
                discount: 0.8,
                desc: @"Computer Networks, 5/e is appropriate for Computer Networking or Introduction to Networking courses at both the undergraduate and graduate level in Computer Science, Electrical Engineering, CIS, MIS, and Business Departments.
Tanenbaum takes a structured approach to explaining how networks work from the inside out. He starts with an explanation of the physical layer of networking, computer hardware and transmission systems; then works his way up to network applications. Tanenbaum's in-depth application coverage includes email; the domain name system; the World Wide Web (both client- and server-side); and multimedia (including voice over IP, Internet radio video on demand, video conferencing, and streaming media. Each chapter follows a consistent approach: Tanenbaum presents key principles, then illustrates them utilizing real-world example networks that run through the entire book�Xthe Internet, and wireless networks, including Wireless LANs, broadband wireless and Bluetooth. The Fifth Edition includes a chapter devoted exclusively to network security. The textbook is supplemented by a Solutions Manual, as well as a Website containing PowerPoint slides, art in various forms, and other tools for instruction, including a protocol simulator whereby students can develop and test their own network protocols.",
                preview: @"Over the course of the years the cable system grew and the cables between the
various cities were replaced by high-bandwidth fiber, similar to what happened in
the telephone system. A system with fiber for the long-haul runs and coaxial
cable to the houses is called an HFC (Hybrid Fiber Coax) system. The electrooptical
converters that interface between the optical and electrical parts of the system
are called fiber nodes. Because the bandwidth of fiber is so much greater
than that of coax, a fiber node can feed multiple coaxial cables. Part of a modern
HFC system is shown in Fig. 2-51(a).
Over the past decade, many cable operators decided to get into the Internet
access business, and often the telephony business as well. Technical differences
between the cable plant and telephone plant had an effect on what had to be done
to achieve these goals. For one thing, all the one-way amplifiers in the system
had to be replaced by two-way amplifiers to support upstream as well as downstream
transmissions. While this was happening, early Internet over cable systems
used the cable television network for downstream transmissions and a dialup
connection via the telephone network for upstream transmissions. It was a
clever workaround, but not much of a network compared to what it could be.",
                out_of_print: false,
                stock: 38,
                category: Category.Science
            );
            return itemDb;
        }
        [Test]
        public void MakeOrderTest()
        {
            MemberSystem memberDb = SetNormalMemberDB();
            CartSystem shoppingCart = new CartSystem();
            ItemSystem itemDb = SetStandardTestItemDB();
            OrderSystem orderDb = new OrderSystem();

            int User1 = memberDb.SignIn("testid2", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            shoppingCart.PutInCart(itemDb.GetItem(0).sn, 2, itemDb.GetItem(0).stock);
            shoppingCart.PutInCart(itemDb.GetItem(1).sn, 4, itemDb.GetItem(1).stock);

            int makeOrder = shoppingCart.Buy(memberDb, orderDb, itemDb);
            Assert.That(makeOrder, Is.EqualTo(0));
            Assert.That(orderDb.GetOrder(0).GetBuyList()[0].itemSN, Is.EqualTo(itemDb.GetItem(0).sn));
            Assert.That(orderDb.GetOrder(0).GetBuyList()[1].itemSN, Is.EqualTo(itemDb.GetItem(1).sn));
            Assert.That(orderDb.GetOrder(0).memberSN, Is.EqualTo(1));
            Assert.That(orderDb.GetOrder(0).receiverName, Is.EqualTo(memberDb.GetMemberName(1)));
            Assert.That(orderDb.GetOrder(0).receiverAddress, Is.EqualTo(memberDb.GetMemberAddress(1)));
            Assert.That(orderDb.GetOrder(0).receiverPhone, Is.EqualTo(memberDb.GetMemberPhone(1)));
        }
        [Test]
        public void CancelOrderTest()
        {
            MemberSystem memberDb = SetNormalMemberDB();
            OrderSystem orderDb = new OrderSystem();
            CartSystem shoppingCart = new CartSystem();
            ItemSystem itemDb = SetStandardTestItemDB();

            int User1 = memberDb.SignIn("testid", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            shoppingCart.PutInCart(itemDb.GetItem(0).sn, 2, itemDb.GetItem(0).stock);

            int orderSN = shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            Assert.That(orderSN, Is.EqualTo(0));
            Assert.That(orderDb.CancelOrder(orderSN), Is.EqualTo(0));
            Assert.That(orderDb.GetOrder(orderSN).canceled, Is.EqualTo(true));
        }
        [Test]
        public void CancelNonExistOrderTest()
        {
            MemberSystem memberDb = SetNormalMemberDB();
            OrderSystem orderDb = new OrderSystem();
            CartSystem shoppingCart = new CartSystem();
            ItemSystem itemDb = SetStandardTestItemDB();

            int User1 = memberDb.SignIn("testid", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            shoppingCart.PutInCart(itemDb.GetItem(0).sn, 2, itemDb.GetItem(0).stock);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            int orderSN = 999;
            Assert.That(orderDb.CancelOrder(orderSN), Is.EqualTo(-1));
        }
        [Test]
        public void MakeOrderWithEmptyCartTest()
        {
            MemberSystem memberDb = SetNormalMemberDB();
            OrderSystem orderDb = new OrderSystem();
            CartSystem shoppingCart = new CartSystem();
            ItemSystem itemDb = SetStandardTestItemDB();

            int User1 = memberDb.SignIn("testid2", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            int makeOrder = shoppingCart.Buy(memberDb, orderDb, itemDb);
            Assert.That(makeOrder, Is.EqualTo(-1));
        }
        [Test]
        public void MakeOrderWithOutOfStockTest()
        {
            MemberSystem memberDb = SetNormalMemberDB();
            OrderSystem orderDb = new OrderSystem();
            CartSystem shoppingCart = new CartSystem();
            ItemSystem itemDb = SetStandardTestItemDB();

            int User1 = memberDb.SignIn("testid2", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            shoppingCart.PutInCart(itemDb.GetItem(0).sn, 2, itemDb.GetItem(0).stock);
            itemDb.GetItem(0).SetStock(1);
            int makeOrder = shoppingCart.Buy(memberDb, orderDb, itemDb);
            Assert.That(makeOrder, Is.EqualTo(-2));
        }
        [Test]
        public void GetOrderTest()
        {
            MemberSystem memberDb = SetNormalMemberDB();
            OrderSystem orderDb = new OrderSystem();
            CartSystem shoppingCart = new CartSystem();
            ItemSystem itemDb = SetStandardTestItemDB();

            int User1 = memberDb.SignIn("testid", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            shoppingCart.PutInCart(itemDb.GetItem(0).sn, 2, itemDb.GetItem(0).stock);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            Assert.That(orderDb.GetOrder(1), Is.Not.EqualTo(null));
        }
        [Test]
        public void GetNonExistOrderTest()
        {
            MemberSystem memberDb = SetNormalMemberDB();
            OrderSystem orderDb = new OrderSystem();
            CartSystem shoppingCart = new CartSystem();
            ItemSystem itemDb = SetStandardTestItemDB();

            int User1 = memberDb.SignIn("testid", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            shoppingCart.PutInCart(itemDb.GetItem(0).sn, 2, itemDb.GetItem(0).stock);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            Assert.That(orderDb.GetOrder(10), Is.EqualTo(null));
        }
        [Test]
        public void PayOrderTest()
        {
            MemberSystem memberDb = SetNormalMemberDB();
            OrderSystem orderDb = new OrderSystem();
            CartSystem shoppingCart = new CartSystem();
            ItemSystem itemDb = SetStandardTestItemDB();

            int User1 = memberDb.SignIn("testid", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            shoppingCart.PutInCart(itemDb.GetItem(0).sn, 2, itemDb.GetItem(0).stock);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            Assert.That(orderDb.PayOrder(1), Is.EqualTo(0));
        }
        [Test]
        public void PayPaidOrderTest()
        {
            MemberSystem memberDb = SetNormalMemberDB();
            OrderSystem orderDb = new OrderSystem();
            CartSystem shoppingCart = new CartSystem();
            ItemSystem itemDb = SetStandardTestItemDB();

            int User1 = memberDb.SignIn("testid", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            shoppingCart.PutInCart(itemDb.GetItem(0).sn, 2, itemDb.GetItem(0).stock);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            orderDb.PayOrder(1);
            Assert.That(orderDb.PayOrder(1), Is.EqualTo(1));
        }
        [Test]
        public void PayNonExistOrderTest()
        {
            MemberSystem memberDb = SetNormalMemberDB();
            OrderSystem orderDb = new OrderSystem();
            CartSystem shoppingCart = new CartSystem();
            ItemSystem itemDb = SetStandardTestItemDB();

            int User1 = memberDb.SignIn("testid", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            shoppingCart.PutInCart(itemDb.GetItem(0).sn, 2, itemDb.GetItem(0).stock);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            Assert.That(orderDb.PayOrder(10), Is.EqualTo(-1));
        }
        [Test]
        public void PickupPaidOrderTest()
        {
            MemberSystem memberDb = SetNormalMemberDB();
            OrderSystem orderDb = new OrderSystem();
            CartSystem shoppingCart = new CartSystem();
            ItemSystem itemDb = SetStandardTestItemDB();

            int User1 = memberDb.SignIn("testid", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            shoppingCart.PutInCart(itemDb.GetItem(0).sn, 2, itemDb.GetItem(0).stock);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            orderDb.PayOrder(1);
            Assert.That(orderDb.PickUpOrder(1), Is.EqualTo(0));
        }
        [Test]
        public void PickupUnPaidOrderTest()
        {
            MemberSystem memberDb = SetNormalMemberDB();
            OrderSystem orderDb = new OrderSystem();
            CartSystem shoppingCart = new CartSystem();
            ItemSystem itemDb = SetStandardTestItemDB();

            int User1 = memberDb.SignIn("testid", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            shoppingCart.PutInCart(itemDb.GetItem(0).sn, 2, itemDb.GetItem(0).stock);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            orderDb.PayOrder(1);
            Assert.That(orderDb.PickUpOrder(2), Is.EqualTo(1));
        }
        [Test]
        public void PickupNonExistOrderTest()
        {
            MemberSystem memberDb = SetNormalMemberDB();
            OrderSystem orderDb = new OrderSystem();
            CartSystem shoppingCart = new CartSystem();
            ItemSystem itemDb = SetStandardTestItemDB();

            int User1 = memberDb.SignIn("testid", "passsssword");
            shoppingCart.SetUserSN(User1, memberDb.GetNumberOfMember());
            shoppingCart.PutInCart(itemDb.GetItem(0).sn, 2, itemDb.GetItem(0).stock);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            shoppingCart.Buy(memberDb, orderDb, itemDb);
            orderDb.PayOrder(1);
            Assert.That(orderDb.PickUpOrder(10), Is.EqualTo(-1));
        }
    }
}
