﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
namespace FinalProject_Team1
{
    [TestFixture]
    public class InvoiceTest
    {
        [Test]
        public void SetInvoiceTest()
        {
            Invoice I1_Donate = new Invoice();
            I1_Donate.SetIsDonate(true);

            Assert.That(I1_Donate.IsDonate, Is.EqualTo(true));

            I1_Donate.SetDonateChoice(1);
            Assert.That(I1_Donate.ChooseDonatePlace(), Is.EqualTo("創世基金會"));

            I1_Donate.SetIsDonate(false);
            Assert.That(I1_Donate.ChooseDonatePlace(), Is.EqualTo("NO Donate"));
        }



    }
}
