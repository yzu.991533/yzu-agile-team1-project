﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject_Team1
{
    class Payment
    {
        public int PayPlaceChoose;
        public string[] PayPlace;


        public Payment()
        {
            PayPlaceChoose = 0;

            PayPlace = new string[] { "ATM轉帳", "超商取貨付款", "住宅貨到付款" };
        }

        public int SetPayPlaceChoose(int UserChoose)
        {
            PayPlaceChoose = UserChoose;
            return PayPlaceChoose;
        }
        public string PaymentPlace()
        {
            return PayPlace[PayPlaceChoose];
        }


    }
}
