﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
namespace FinalProject_Team1
{
    [TestFixture]
    class ProductTransportTest
    {
        [Test]
        public void SetProductTransportTest()
        {
            ProductTransport Payplace_Return = new ProductTransport();
            Payplace_Return.SetProductChoice(0);
            Assert.That(Payplace_Return.ChooseProductPlace(), Is.EqualTo("宅配取貨"));

            Payplace_Return.SetProductChoice(1);
            Assert.That(Payplace_Return.ChooseProductPlace(), Is.EqualTo("超商取貨"));
        }

    }
}
