﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject_Team1
{
    class ProductTransport
    {
        public int TransportChoice { get; private set; }
        public string[] TransportPlace { get; private set; }

        public ProductTransport()
        {
            TransportChoice = 0;
            TransportPlace = new String[] {"宅配取貨", "超商取貨" };

        }

        public void SetProductChoice(int choice)
        {
            TransportChoice = choice;
        }

        public string ChooseProductPlace()
        {
                return TransportPlace[TransportChoice];
        }


    }
}
